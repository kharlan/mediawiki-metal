Patches welcome!

We track tasks on [Phabricator #MediaWiki-Metal](https://phabricator.wikimedia.org/project/view/6545/)

Please run `git config commit.template .gitmessage` to use the project's git message template.
