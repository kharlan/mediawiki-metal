# MediaWiki Metal

A framework for running a MediaWiki development environment on "bare metal".

## Prerequisites

### macOS

- Install [Homebrew](https://brew.sh/): `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- `brew install php@8.1`
- `brew install git`
- [recommended] `brew install mariadb`
- [recommended] `brew install httpd`

## Install

- Clone this repository, e.g. `git clone https://gitlab.wikimedia.org/kharlan/mediawiki-metal.git ~/src/mw-metal`
- Add `/path/to/mw-metal/bin` to your `$PATH`, e.g. `export PATH="/Users/{username}/src/mw-metal/bin:$PATH"`
- Run `composer install` in the repository

## Set up MediaWiki

- `mw-metal setup`

## Usage

| Command           | Description                                   |
|-------------------|-----------------------------------------------|
| `mw-metal status` | Show status of local environment installation |
| `mw-metal setup`  | Initial setup of local site                   |

## Philosophy

- **Bare metal**: Use host system package manager for PHP interpreter, HTTP server, and database
- **High performance**: Compared to virtualization, host system software has fewer barriers to high performance
- **Avoid reinventing wheels**: Use MediaWiki core scripts for functionality that is useful to other development environments (T333668)
- **Keep it simple**: The environment aims to meet the needs of developers who want to do common tasks in MediaWiki core and extension development
- **Easy to debug**: Errors should be intelligible with clear directions to debug and fix problems
- **Easy to contribute to**: This framework is in PHP as the common language of the MediaWiki ecosystem
- **Avoid configuration where possible**: Configuration escalates complexity and makes it more difficult to support users
