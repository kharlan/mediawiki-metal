<?php

namespace Kostajh\MwMetal\Command;

use Kostajh\MwMetal\Config\ConfigReader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class ConfigGetCommand extends Command {

	private ConfigReader $configReader;

	/**
	 * @param string|null $name
	 * @param ConfigReader $configReader
	 */
	public function __construct( ?string $name, ConfigReader $configReader ) {
		parent::__construct( $name );
		$this->configReader = $configReader;
	}

	public function configure() {
		$this->setName( 'config:get' );
		$this->setDescription( 'Get configuration settings for MediaWiki Metal' );
		$this->addArgument(
			'component_name',
			InputOption::VALUE_REQUIRED,
			'Name of the config to fetch',
			null
		);
		$this->addOption(
			'format',
			null,
			InputOption::VALUE_REQUIRED,
			'Output format to use. Valid options are: "table" and "json".',
			'table'
		);
	}

	/** @inheritDoc */
	public function execute( InputInterface $input, OutputInterface $output ) {
		$config = $this->configReader->read();
		$componentName = $input->getArgument( 'component_name' );
		$io = new SymfonyStyle( $input, $output );
		if ( $componentName ) {
			if ( !isset( $config[$componentName] ) ) {
				$output->writeln( '<error>Config component ' . $componentName . ' does not exist.</error>' );
				return Command::FAILURE;
			}
			$io->writeln( $config[$componentName] );
			return Command::SUCCESS;
		}
		if ( $input->getOption( 'format' ) === 'json' ) {
			$io->writeln( json_encode( $config ) );
			return Command::SUCCESS;
		}
		$yaml = Yaml::dump( $config );
		$io->write( $yaml );
		return Command::SUCCESS;
	}
}
