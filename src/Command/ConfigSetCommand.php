<?php

namespace Kostajh\MwMetal\Command;

use Kostajh\MwMetal\Config\ConfigWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigSetCommand extends Command {

	public const CONFIG_VERSION = 1;
	private ConfigWriter $configWriter;

	/**
	 * @param string|null $name
	 * @param ConfigWriter $configWriter
	 */
	public function __construct( ?string $name, ConfigWriter $configWriter ) {
		parent::__construct( $name );
		$this->configWriter = $configWriter;
	}

	public function configure() {
		$this->setName( 'config:set' );
		$this->addArgument( 'component_name' );
		$this->addArgument( 'component_value' );
	}

	/** @inheritDoc */
	public function execute( InputInterface $input, OutputInterface $output ): int {
		$config = [
			$input->getArgument( 'component_name' ) =>
				$input->getArgument( 'component_value' )
		];
		$this->configWriter->write( $config );
		return Command::SUCCESS;
	}

}
