<?php

namespace Kostajh\MwMetal\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class PhpWebserverCommand extends Command {

	protected function configure() {
		$this->setName( 'php-webserver' );
		$this->setDescription( 'Start "php -S" pointing to MediaWiki directory.' );
		$this->addOption(
			'port',
			'p',
			InputOption::VALUE_REQUIRED,
			'The port to use for serving development environment site(s).',
			8080
		);
		$this->addOption(
			'directory',
			null,
			InputOption::VALUE_REQUIRED,
			'The path to MediaWiki core',
			getenv( 'HOME' ) . '/src/mediawiki/w'
		);
		$this->addArgument( 'subcommand', InputArgument::OPTIONAL );
	}

	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		if ( $input->getArgument( 'subcommand' ) === 'stop' ) {
			$io->info( 'Stopping PHP built-in webserver' );
			$this->stop();
			$io->success( 'Successfully stopped PHP built-in server.' );
			return Command::SUCCESS;
		}
		if ( !in_array( $input->getArgument( 'subcommand' ), [ 'start', 'stop' ] ) ) {
			$io->error( 'You need to use "start" or "stop" with this command.' );
			return Command::FAILURE;
		}

		$io->info( 'Starting PHP built-in webserver' );
		$port = $input->getOption( 'port' );
		$process = new Process(
			[
				'php',
				'-S',
				'localhost:' . $port,
				'maintenance/dev/includes/router.php'
			],
			$input->getOption( 'directory' ),
			[ 'PHP_CLI_SERVER_WORKERS' => '3' ]
		);
		$process->setOptions( [ 'create_new_console' => true ] );
		$process->start();
		$io->success( 'Successfully started PHP built-in webserver on http://localhost:' . $port );
		return Command::SUCCESS;
	}

	private function stop() {
		$process = new Process( [
			'pkill',
			'-f',
			'php',
			'-S',
			'maintenance/dev/includes/router.php'
		] );
		$process->run();
	}

}
