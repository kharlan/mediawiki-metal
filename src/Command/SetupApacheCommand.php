<?php

namespace Kostajh\MwMetal\Command;

use Kostajh\MwMetal\OS;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class SetupApacheCommand extends Command {

	protected function configure() {
		$this->setName( 'setup:apache' );
		$this->setDescription( 'Configure httpd.conf, and enable VirtualHosts' );
		$this->addOption(
			'port',
			'p',
			InputOption::VALUE_REQUIRED,
			'The port to use for serving development environment site(s).',
			80
		);
		$this->addOption(
			'document-root',
			null,
			InputOption::VALUE_REQUIRED,
			'The path to use as the DocumentRoot with Apache. 
			This should be one directory above the MediaWiki core repository.',
			getenv( 'HOME' ) . '/src/mediawiki'
		);
	}

	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		$httpdVhostsContents = file_get_contents(
			dirname( __FILE__, 3 ) . '/resources/httpd/httpd-vhosts.conf'
		);
		$httpdVhostsContents = str_replace(
			[
				'{MW_METAL_DOCUMENTROOT}',
				'{MW_METAL_PORT}',
			],
			[
				$input->getOption( 'document-root' ),
				$input->getOption( 'port' ),
			],
			$httpdVhostsContents
		);
		// TODO: Support Debian.
		$apacheVars = $this->getApachePaths();

		# Linux workaround stuff
		if ( OS::Linux->value === $this->getOS() ) {
			$httpdVhostsContents = str_replace(
				[
					'ErrorLog "/opt/homebrew/var/log/httpd/default.mediawiki.localhost-error_log"',
					'CustomLog "/opt/homebrew/var/log/httpd/default.mediawiki.localhost-access_log" common',
				],
				[
					'ErrorLog ${APACHE_LOG_DIR}/error.log',
					'ErrorLog ${APACHE_LOG_DIR}/access.log',
				],
			$httpdVhostsContents );
			file_put_contents( '/tmp/metal-vhost.conf', $httpdVhostsContents );

			# Skip moving the apache config and restarting the server to avoid permissions issues
			$io->success( 'Successfully created apache config!' );
			return Command::SUCCESS;
		}

		$httpdVhostsFile = $apacheVars['vhost-conf'];

		if ( file_exists( $httpdVhostsFile ) ) {
			$httpdVhostsFileBackup = $httpdVhostsFile . '.' . date( 'Y-m-D-H:i:s' );
			copy( $httpdVhostsFile, $httpdVhostsFileBackup );
			if ( $output->isVeryVerbose() ) {
				$io->info( 'Backing up existing httpd-vhosts.conf file to ' . $httpdVhostsFileBackup );
			}
		}
		$io->info( "Replacing $httpdVhostsFile" );

		# TODO: this needs root perms on linux. need to figure this out.
		file_put_contents( $httpdVhostsFile, $httpdVhostsContents );

		$httpdConfFile = $apacheVars['main-conf'];
		if ( file_exists( $httpdConfFile ) ) {
			$httpdConfFileBackup = $httpdConfFile . '.' . date( 'Y-m-D-H:i:s' );
			copy( $httpdConfFile, $httpdConfFileBackup );
			if ( $output->isVeryVerbose() ) {
				$io->comment( 'Backing up existing httpd.conf file to ' . $httpdConfFileBackup );
			}
		}
		$io->info( 'Modifying httpd/httpd.conf to enable VirtualHosts' );

		if ( $this->getOS() === OS::Mac->value ) {
			$this->macEnableApacheModules( $httpdConfFile );
		}

		$process = new Process( [ $apacheVars['service-name'], '-S' ] );
		$process->run();
		if ( $output->isVeryVerbose() ) {
			$io->comment( $process->getOutput() );
		}
		if ( $process->getExitCode() !== 0 ) {
			$io->error( 'Invalid httpd configuration.' );
			$io->error( $process->getErrorOutput() );
			return Command::FAILURE;
		}
		$io->info( 'Restarting httpd' );
		$process = new Process( [ 'brew', 'services', 'restart', 'httpd' ] );
		$process->run();
		if ( $process->getExitCode() === 0 ) {
		} else {
			$io->error( $process->getErrorOutput() );
		}

		$io->success( 'Successfully set up httpd' );
		return Command::SUCCESS;
	}

	protected function getApachePaths(): array {
		$os = $this->getOS();
		return match ( $os ) {
			OS::Linux->value => [
				'vhost-conf' => '/etc/apache2/sites-available/metal.conf',
				'main-conf' => '/etc/apache2/apache2.conf',
				'service-name' => 'apache2'
			],
			OS::Mac->value => [
				'vhost-conf' => getenv( 'HOMEBREW_PREFIX' ) . '/etc/httpd/extra/httpd-vhosts.conf',
				'main-conf' => getenv( 'HOMEBREW_PREFIX' ) . '/etc/httpd/httpd.conf',
				'service-name' => 'httpd'
			],
			default => [
				'vhost-conf' => '/path/default',
				'main-conf' => '/path/default',
				'service-name' => 'httpd'
			],
		};
	}

	/**
	 * Return the current user's Operating System
	 */
	protected function getOS(): string {
		return php_uname( 's' );
	}

	/**
	 * @param string $httpdConfFile
	 *
	 * @return void
	 */
	protected function macEnableApacheModules( string $httpdConfFile ): void {
		$httpdConfContents = file_get_contents( $httpdConfFile );
		$httpdConfContents = str_replace( [
			'#LoadModule vhost_alias_module lib/httpd/modules/mod_vhost_alias.so',
			'#LoadModule rewrite_module lib/httpd/modules/mod_rewrite.so',
			'#LoadModule log_config_module lib/httpd/modules/mod_log_config.so',
			'#Include ' . getenv( 'HOMEBREW_PREFIX' ) . '/etc/httpd/extra/httpd-vhosts.conf',
			'Listen 80',
		], [
			'LoadModule vhost_alias_module lib/httpd/modules/mod_vhost_alias.so',
			'LoadModule rewrite_module lib/httpd/modules/mod_rewrite.so',
			'LoadModule log_config_module lib/httpd/modules/mod_log_config.so',
			'Include ' . getenv( 'HOMEBREW_PREFIX' ) . '/etc/httpd/extra/httpd-vhosts.conf',
			'#Listen 80',
		], $httpdConfContents );
		$loadModuleLibPhp = 'LoadModule php_module $HOMEBREW_PREFIX/opt/php/lib/httpd/modules/libphp.so';
		if ( !str_contains( $httpdConfContents, $loadModuleLibPhp ) ) {
			$httpdConfContents .= PHP_EOL . $loadModuleLibPhp . PHP_EOL;
		}
		file_put_contents( $httpdConfFile, $httpdConfContents );
	}

}
