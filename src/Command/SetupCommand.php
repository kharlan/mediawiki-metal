<?php

namespace Kostajh\MwMetal\Command;

use Kostajh\MwMetal\Config\ConfigReader;
use Kostajh\MwMetal\Config\ConfigWriter;
use Kostajh\MwMetal\OS;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetupCommand extends Command {
	private ConfigWriter $configWriter;
	private ConfigReader $configReader;

	/**
	 * @param string|null $name
	 * @param ConfigWriter $configWriter
	 * @param ConfigReader $configReader
	 */
	public function __construct( ?string $name, ConfigWriter $configWriter, ConfigReader $configReader ) {
		parent::__construct( $name );
		$this->configWriter = $configWriter;
		$this->configReader = $configReader;
	}

	private const STAGES = [
		'mediawiki-download',
		'apache',
		'mediawiki',
		'status',
	];

	protected function configure() {
		$this->setName( 'setup' );
		$this->setDescription( 'Setup development environment.' );

		$this->addOption(
			'developer-username',
			'u',
			InputOption::VALUE_OPTIONAL,
			'The developer account username to use with cloning MediaWiki.',
			''
		);
		$this->addOption(
			'directory',
			null,
			InputOption::VALUE_REQUIRED,
			'The directory to use for cloning the MediaWiki core repository.',
			getenv( 'HOME' ) . '/src/mediawiki'
		);
		$this->addOption(
			'url',
			null,
			InputOption::VALUE_REQUIRED,
			'The URL to use for the MediaWiki site installation',
			'http://default.mediawiki.localhost'
		);
		$this->addOption(
			'port',
			'p',
			InputOption::VALUE_REQUIRED,
			'The port to use for serving development environment site(s).',
			80
		);
		$this->addOption(
			'clone-with-anonymous-https',
			null,
			InputOption::VALUE_NONE,
			'Whether to use anonymous HTTPS when cloning MediaWiki',
		);
		$this->addOption(
			'clone-depth',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to use with --depth for git clone commands',
			null
		);
		$this->addOption(
			'stages',
			null,
			InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
			implode( " ", self::STAGES ),
			self::STAGES
		);
		$this->addOption(
			'webserver-backend',
			null,
			InputOption::VALUE_REQUIRED,
			'Whether to use apache or php built-in webserver as the webserver backend.',
		);
		$this->addOption(
			'script-path',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to use for $wgScriptPath in install.php',
		);
		$this->addOption(
			'db-type',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to pass to --dbtype for install.php',
			'mysql'
		);
	}

	/** @inheritDoc */
	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$app = $this->getApplication();
		$stages = $input->getOption( 'stages' );
		$io = new SymfonyStyle( $input, $output );

		$config = $this->configReader->read();
		if ( $config ) {
			$io->warning( 'Configuration already exists in ~/.mw-metal.yml' );
			$helper = $this->getHelper( 'question' );
			$question = new ConfirmationQuestion( 'Continue with setup? (y/n) ', false );
			if ( !$helper->ask( $input, $output, $question ) ) {
				return Command::SUCCESS;
			}
		}

		$directory = $config['directory'] ?? $input->getOption( 'directory' );
		$developerUsername = $config['developer_username'] ?? $input->getOption( 'developer-username' );

		if ( file_exists( $directory . '/w' ) ) {
			// phpcs:disable Generic.Files.LineLength
			$io->warning( 'Directory' . $directory . ' already exists.  Skipping MediaWiki download stage.' );
			unset( $stages[ 'mediawiki-download'] );
		} elseif ( in_array( 'mediawiki-download', $stages ) ) {
			$options = [
				'--developer-username' => $developerUsername,
				'--directory' => $directory,
				'--document-root' => $directory,
			];
			if ( $input->getOption( 'clone-depth' ) ) {
				$options = array_merge( $options, [ '--clone-depth' => $input->getOption( 'clone-depth' ) ] );
			}
			if ( $input->getOption( 'clone-with-anonymous-https' ) ) {
				$options = array_merge( $options, [ '--clone-with-anonymous-https' => $input->getOption( 'clone-with-anonymous-https' ) ] );
			}

			$setupMediaWikiDownloadCommandResult = $app->find( 'setup:mediawiki-download' )->run(
				new ArrayInput( $options ),
				$output
			);
			if ( $setupMediaWikiDownloadCommandResult !== 0 ) {
				return Command::FAILURE;
			}
		}

		$webserverBackend = $config['webserver_backend'] ?? $input->getOption( 'webserver-backend' );
		$port = $config['port'] ?? $input->getOption( 'port' );

		if ( $webserverBackend !== 'php' &&
			( in_array( 'apache', $stages ) || $webserverBackend === 'apache' ) ) {
			$setupApacheCommandResult = $app->find( 'setup:apache' )->run(
				new ArrayInput( [
					'--port' => $port,
					'--document-root' => $directory,
				] ),
				$output
			);
			if ( $setupApacheCommandResult !== 0 ) {
				return Command::FAILURE;
			}
		}

		if ( in_array( 'php-webserver', $stages ) || $webserverBackend ) {
			$io->info( 'Starting PHP server' );
			// HACK Would be nice to use Process, like we do in PhpWebserverCommand.
			// But that isn't working here.
			// phpcs:ignore
			shell_exec( 'bin/mw-metal php-webserver start --directory=' . $directory . ' --port=' . $port );
		}

		$url = $config['url'] ?? $input->getOption( 'url' );
		$dbType = $config['db_type'] ?? $input->getOption( 'db-type' );
		if ( file_exists( $directory . '/w/LocalSettings.php' ) ) {
			$io->warning( 'LocalSettings.php exists, skipping install phase.' );
			unset( $stages['mediawiki'] );
		} elseif ( in_array( 'mediawiki', $stages ) ) {
			$setupMediaWikiCommandResult = $app->find( 'setup:mediawiki' )->run(
				new ArrayInput( [
					'--url' => $url,
					'--directory' => $directory . '/w',
					'--script-path' => $webserverBackend === 'php' ? "" : '/w',
					'--db-type' => $dbType
				] ),
				$output
			);
			if ( $setupMediaWikiCommandResult !== 0 ) {
				return Command::FAILURE;
			}
		}

		$this->configWriter->write( [
			'developer_username' => $developerUsername,
			'directory' => $directory,
			'url' => $url,
			'port' => $port,
			'stages' => $input->getOption( 'stages' ),
			'webserver_backend' => $webserverBackend,
			'script_path' => $input->getOption( 'script-path' ),
			'db_type' => $dbType
		] );

		if ( in_array( 'status', $stages ) ) {
			$statusCommandResult = $app->find( 'status' )->run( new ArrayInput( [ '--format' => 'table' ] ), $output );
			if ( $statusCommandResult !== 0 ) {
				return Command::FAILURE;
			}
		}

		if ( $this->getOS() === OS::Linux->value &&
			$webserverBackend !== 'php' &&
			( in_array( 'apache', $stages ) || $webserverBackend === 'apache' )
		) {
			# output linux instructions for setting up apache
			$output->writeln( 'Please run the following commands to configure apache:' );
			$output->writeln( 'sudo cp /tmp/metal-vhost.conf /etc/apache2/sites-enabled/metal-vhost.conf ' );
			$output->writeln( 'sudo service apache2 restart ' );
		}
		return Command::SUCCESS;
	}

	/**
	 * Return the current user's Operating System
	 */
	protected function getOS(): string {
		return php_uname( 's' );
	}

}
