<?php

namespace Kostajh\MwMetal\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class SetupComposerCommand extends Command {

	public function configure() {
		$this->setName( 'setup:composer' );
		$this->setDescription( 'Download and install composer, if needed' );
	}

	public function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		$io->info( 'Checking if "composer" is installed' );
		$process = new Process( [ 'which', 'composer' ] );
		$process->run();
		if ( $process->getExitCode() === 0 ) {
			$io->success( 'composer is already installed' );
			return Command::SUCCESS;
		} else {
			$io->caution( 'composer is not installed; attempting to install' );
		}
		$io->info( 'Installing "composer"' );
		$process = new Process( [ 'sh', dirname( __FILE__, 3 ) . '/resources/composer-installer.sh' ] );
		$process->run();
		if ( $process->getExitCode() !== 0 ) {
			$io->error( 'Failed to install composer.' );
			$io->error( $process->getErrorOutput() );
			return Command::FAILURE;
		}
		$process = new Process( [ 'composer', '--version' ] );
		$process->run();
		$io->success( 'Successfully installed composer: ' . $process->getOutput() );
		return Command::SUCCESS;
	}
}
