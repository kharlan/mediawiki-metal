<?php

namespace Kostajh\MwMetal\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class SetupMediaWikiCommand extends Command {

	public function configure() {
		$this->setName( 'setup:mediawiki' );
		$this->setDescription( 'Install MediaWiki' );
		$this->addOption(
			'directory',
			'-d',
			InputOption::VALUE_REQUIRED,
			'The location of the MediaWiki core repository',
			getenv( 'HOME' ) . '/src/mediawiki/w'
		);
		$this->addOption(
			'url',
			null,
			InputOption::VALUE_REQUIRED,
			'The URL to use for the MediaWiki site installation',
			'http://default.mediawiki.localhost'
		);
		$this->addOption(
			'script-path',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to use for $wgScriptPath',
		);
		$this->addOption(
			'db-type',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to pass to --dbtype for install.php',
			'mysql'
		);
	}

	public function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		$directory = $input->getOption( 'directory' );
		if ( !file_exists( $directory . '/composer.local.json' ) ) {
			copy( dirname( __FILE__, 3 ) . '/resources/composer.local.json',  $directory . '/composer.local.json' );
		}
		$io->info( 'Running "composer update"' );
		$process = new Process( [ 'composer', 'update' ], $directory );
		$process->setTimeout( null );
		$process->run();
		if ( $process->getExitCode() === 0 ) {
			$io->success( 'Successfully ran "composer update"' );
		} else {
			$io->error( 'Failed to run "composer update"' );
			$io->error( $process->getErrorOutput() );
			return Command::FAILURE;
		}

		// TODO: Handle case if LocalSettings.php already exists.
		$io->info( 'Running MediaWiki install.php script' );
		copy(
			dirname( __FILE__, 3 ) . '/resources/PlatformSettings.php',
			$directory . '/includes/PlatformSettings.php'
		);
		if ( $input->getOption( 'script-path' ) === '/w' ) {
			file_put_contents(
				$directory . '/includes/PlatformSettings.php',
				'$wgArticlePath = "/wiki/$1";' . PHP_EOL,
				FILE_APPEND );
		}
		$process = new Process( [
			'php',
			'maintenance/run.php',
			'install',
			'Default-MediaWiki',
			'Admin',
			'--dbtype=' . $input->getOption( 'db-type' ),
			'--dbname=default_mediawiki',
			'--dbuser=root',
			'--dbserver=127.0.0.1',
			'--skins=Vector',
			'--scriptpath=' . $input->getOption( 'script-path' ) ?? '',
			'--server=' . $input->getOption( 'url' ),
			'--pass=Metal_letaM'
		], $directory );
		$process->run();
		$io->success( 'Successfully ran MediaWiki\'s install.php' );
		if ( $io->isVerbose() ) {
			$io->comment( $process->getOutput() );
		}
		if ( $process->getExitCode() !== 0 ) {
			$io->error( $process->getErrorOutput() );
			return Command::FAILURE;
		}
		$io->horizontalTable(
			[ 'Username', 'Password', 'URL' ],
			[
				[ 'Admin' ],
				[ 'Metal_letaM' ],
				[ $input->getOption( 'url' ) ]
			]
		);
		return Command::SUCCESS;
	}

}
