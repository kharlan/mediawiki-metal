<?php

namespace Kostajh\MwMetal\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class SetupMediaWikiDownloadCommand extends Command {

	public function configure() {
		$this->setName( 'setup:mediawiki-download' );
		$this->setDescription( 'Download (clone) MediaWiki core' );
		$this->addOption(
			'developer-username',
			'u',
			InputOption::VALUE_OPTIONAL,
			'The developer account username to use with cloning MediaWiki.',
			''
		);
		$this->addOption(
			'directory',
			null,
			InputOption::VALUE_REQUIRED,
			'The directory to use for cloning the MediaWiki core repository.',
			getenv( 'HOME' ) . '/src/mediawiki'
		);
		$this->addOption(
			'clone-with-anonymous-https',
			null,
			InputOption::VALUE_NONE,
			'Whether to use anonymous HTTPS when cloning MediaWiki',
		);
		$this->addOption(
			'document-root',
			null,
			InputOption::VALUE_REQUIRED,
			'The path to use as the DocumentRoot with Apache.
			This should be one directory above the MediaWiki core repository.',
			getenv( 'HOME' ) . '/src/mediawiki'
		);
		$this->addOption(
			'clone-depth',
			null,
			InputOption::VALUE_REQUIRED,
			'Value to use with --depth for git clone commands',
			null
		);
	}

	public function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		$directory = $input->getOption( 'directory' );
		$mediaWikiCoreDirectory = $directory . '/w';
		if ( $io->isVerbose() ) {
			$io->comment( 'Directory: ' . $directory );
		}
		if ( file_exists( $mediaWikiCoreDirectory ) ) {
			$io->error( "Directory $mediaWikiCoreDirectory already exists." );
			return Command::FAILURE;
		}
		if ( $input->getOption( 'clone-with-anonymous-https' ) ) {
			$cloneUrl = "https://gerrit.wikimedia.org/r/mediawiki/core";
		} else {
			$cloneUrl = 'ssh://<USERNAME>@gerrit.wikimedia.org:29418/mediawiki/core';
			$developerUsername = $input->getOption( 'developer-username' );
			if ( $developerUsername ) {
				$developerUsername .= '@';
			}
			$cloneUrl = str_replace(
				'<USERNAME>@',
				$developerUsername,
				$cloneUrl
			);
		}
		if ( $io->isVerbose() ) {
			$io->comment( 'Clone URL: ' . $cloneUrl );
			$io->comment( 'Cloning to: ' . $mediaWikiCoreDirectory );
		}
		$io->info( 'Cloning mediawiki/core' );
		$gitCommand = [ 'git', 'clone', '-v' ];
		if ( $input->getOption( 'clone-depth' ) ) {
			$gitCommand[] = '--depth=' . $input->getOption( 'clone-depth' );
		}
		$process = new Process( array_merge( $gitCommand, [ $cloneUrl, $mediaWikiCoreDirectory ] ) );
		$process->setTimeout( null );
		$process->run();
		if ( $process->getExitCode() !== 0 ) {
			$io->error( 'Failed to clone MediaWiki.' );
			$io->error( $process->getErrorOutput() );
			return Command::FAILURE;
		}
		$io->success( 'Successfully cloned MediaWiki core!' );

		// TODO: Eventually use MediaWiki core maintenance script for doing this T333668
		$io->info( 'Cloning Vector' );
		$cloneUrl = str_replace( 'mediawiki/core', 'mediawiki/skins/Vector', $cloneUrl );

		$gitCommand = [ 'git', 'clone', '-v' ];
		if ( $input->getOption( 'clone-depth' ) ) {
			$gitCommand[] = '--depth=' . $input->getOption( 'clone-depth' );
		}
		$process = new Process( array_merge( $gitCommand, [ $cloneUrl, $mediaWikiCoreDirectory . '/skins/Vector' ] ) );
		$process->setTimeout( null );
		$process->run();
		$io->success( 'Successfully cloned Vector' );
		return Command::SUCCESS;
	}
}
