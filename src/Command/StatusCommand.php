<?php

namespace Kostajh\MwMetal\Command;

use Kostajh\MwMetal\Status\StatusInterface;
use Kostajh\MwMetal\Status\StatusLookup;
use LogicException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StatusCommand extends Command {

	protected function configure() {
		$this->setName( 'status' );
		$this->setDescription( 'Display status of MediaWiki installation and host system components.' );
		$this->addOption(
			'format',
			'f',
			InputOption::VALUE_OPTIONAL,
			'Output format to use with the command. Valid options are "table" and "json".',
			'table',
		);
	}

	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
		$statusLookup = new StatusLookup();
		$data = $statusLookup->getAll();
		if ( $input->getOption( 'format' ) === 'json' ) {
			$output->writeln( json_encode( $data ) );
			return Command::SUCCESS;
		}
		$rows = [];
		foreach ( $data as $info ) {
			$rows[] = $this->formatInfoForOutput( $info );
		}
		$io->table( [ 'Component', 'Value', 'OK', 'Note' ], $rows );
		return Command::SUCCESS;
	}

	private function formatInfoForOutput( array $info ): array {
		switch ( $info['ok'] ) {
			case StatusInterface::STATUS_OK:
				$info['ok'] = '✅';
				break;
			case StatusInterface::STATUS_WARNING:
				$info['ok'] = '⚠️';
				break;
			case StatusInterface::STATUS_ERROR:
				$info['ok'] = '❌';
				break;
			default:
				throw new LogicException( 'Invalid status: ' . $info['ok'] );
		}
		if ( !$info['note'] ) {
			$info['note'] = '–';
		}
		if ( !$info['value'] ) {
			$info['value'] = '–';
		}
		return $info;
	}
}
