<?php

namespace Kostajh\MwMetal\Config;

use Symfony\Component\Yaml\Yaml;

class ConfigReader {

	public function read(): ?array {
		$yamlFile = $_SERVER['HOME'] . '/.mw-metal.yml';
		if ( file_exists( $yamlFile ) ) {
			return Yaml::parseFile( $yamlFile ) ?? [];
		}
		return null;
	}
}
