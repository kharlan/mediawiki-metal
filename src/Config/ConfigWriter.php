<?php

namespace Kostajh\MwMetal\Config;

use Symfony\Component\Yaml\Yaml;

class ConfigWriter {

	private ConfigReader $configReader;

	public function __construct( ConfigReader $configReader ) {
		$this->configReader = $configReader;
	}

	/**
	 * @param array $config
	 * @return void
	 */
	public function write( array $config ) {
		$existingConfig = $this->configReader->read() ?? [];
		$newConfig = array_replace( $existingConfig, $config );
		$yaml = Yaml::dump( $newConfig );
		file_put_contents( $_SERVER['HOME'] . '/.mw-metal.yml', $yaml );
	}

}
