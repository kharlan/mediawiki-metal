<?php

namespace Kostajh\MwMetal;

enum OS : string
{
	case Mac = 'Darwin';
	case Linux = 'Linux';

}
