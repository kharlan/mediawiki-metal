<?php

namespace Kostajh\MwMetal\Status;

use Symfony\Component\Process\Process;

class Apache implements StatusInterface {

	public function compute(): StatusLookupValue {
		$process = new Process( [ 'httpd', '-v' ] );
		$process->run();
		$value = trim( $process->getOutput() );
		$value = explode( "\n", $value )[0];
		return new StatusLookupValue(
			'Apache',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "brew install httpd" or "apt install httpd" to install and run Apache.'
		);
	}
}
