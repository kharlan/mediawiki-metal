<?php

namespace Kostajh\MwMetal\Status;

use Symfony\Component\Process\Process;

class Composer implements StatusInterface {

	public function compute(): StatusLookupValue {
		$process = new Process( [ 'composer', '--version' ] );
		$process->run();
		$value = trim( $process->getOutput() );
		$note = '–';
		$ok = self::STATUS_OK;
		if ( $process->getExitCode() !== 0 ) {
			$note = 'See https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos';
			$ok = self::STATUS_ERROR;
			$value = '–';
		}
		return new StatusLookupValue(
			'Composer',
			$value,
			$ok,
			$note
		);
	}
}
