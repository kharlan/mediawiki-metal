<?php

namespace Kostajh\MwMetal\Status;

use Symfony\Component\Process\Process;

class Git implements StatusInterface {

	public function compute(): StatusLookupValue {
		$process = new Process( [ 'git', '-v' ] );
		$process->run();
		$value = trim( $process->getOutput() );
		return new StatusLookupValue(
			'Git',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "brew install git" or "apt install git" to install git.'
		);
	}

}
