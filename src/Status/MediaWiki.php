<?php

namespace Kostajh\MwMetal\Status;

use Kostajh\MwMetal\Config\ConfigReader;
use Symfony\Component\Process\Process;

class MediaWiki implements StatusInterface {

	public function compute(): StatusLookupValue {
		$configReader = new ConfigReader();
		$config = $configReader->read();
		if ( !$config ) {
			return new StatusLookupValue(
				'MediaWiki',
				'MediaWiki Metal is not managing a MediaWiki instance.',
				self::STATUS_ERROR,
				'Run "mw-metal setup"'
			);
		}
		$mwCore = $config['directory'] . '/w';
		$process = new Process( [ 'php', $mwCore . '/maintenance/run.php', 'version' ] );
		$process->run();
		$value = str_replace( 'MediaWiki version: ', '', trim( $process->getOutput() ) );
		$value .= " ($mwCore)";
		return new StatusLookupValue(
			'MediaWiki',
			$value,
			self::STATUS_OK,
			'-'
		);
	}
}
