<?php

namespace Kostajh\MwMetal\Status;

use Symfony\Component\Process\Process;

class MySQL implements StatusInterface {

	public function compute(): StatusLookupValue {
		$process = new Process( [ 'mysql', '--version' ] );
		$process->run();
		$value = trim( $process->getOutput() );
		return new StatusLookupValue(
			'MySQL',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "brew install mariadb" or "apt install mariadb" to install and run MySQL.'
		);
	}
}
