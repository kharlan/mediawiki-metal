<?php

namespace Kostajh\MwMetal\Status;

class PHP implements StatusInterface {

	public function compute(): StatusLookupValue {
		return new StatusLookupValue(
			'PHP',
			phpversion()
		);
	}

}
