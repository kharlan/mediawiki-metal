<?php

namespace Kostajh\MwMetal\Status;

class PhpMemcached implements StatusInterface {

	public function compute(): StatusLookupValue {
		$value = phpversion( 'memcached' );
		return new StatusLookupValue(
			'PHP extension: Memcached',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "pecl install memcached" to install and enable the Memcached extension.'
		);
	}
}
