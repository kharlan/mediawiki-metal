<?php

namespace Kostajh\MwMetal\Status;

class PhpRedis implements StatusInterface {

	public function compute(): StatusLookupValue {
		$value = phpversion( 'redis' );
		return new StatusLookupValue(
			'PHP extension: Redis',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "pecl install redis" to install and enable the Redis extension.'
		);
	}
}
