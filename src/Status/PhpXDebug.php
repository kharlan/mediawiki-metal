<?php

namespace Kostajh\MwMetal\Status;

class PhpXDebug implements StatusInterface {

	public function compute(): StatusLookupValue {
		$value = phpversion( 'xdebug' );
		return new StatusLookupValue(
			'PHP extension: XDebug',
			$value,
			$value ? self::STATUS_OK : self::STATUS_WARNING,
			$value ? '–' : 'Run "pecl install xdebug" to install and enable the XDebug extension.'
		);
	}
}
