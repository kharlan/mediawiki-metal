<?php

namespace Kostajh\MwMetal\Status;

interface StatusInterface {

	public const STATUS_OK = 0;
	public const STATUS_WARNING = 1;
	public const STATUS_ERROR = 2;

	public function compute(): StatusLookupValue;

}
