<?php

namespace Kostajh\MwMetal\Status;

class StatusLookup {

	private const STATUS_LOOKUPS = [
		'mediawiki' => MediaWiki::class,
		'php' => PHP::class,
		'xdebug' => PhpXDebug::class,
		'redis' => PhpRedis::class,
		'memcached' => PhpMemcached::class,
		'mysql' => MySQL::class,
		'httpd' => Apache::class,
		'composer' => Composer::class,
		'git' => Git::class,
	];

	public function getAll(): array {
		$data = [];
		foreach ( self::STATUS_LOOKUPS as $lookup ) {
			/** @var StatusInterface $lookupClass */
			$lookupClass = new $lookup();
			$value = $lookupClass->compute();
			$data[] = [
				'name' => $value->getName(),
				'value' => $value->getValue(),
				'ok' => $value->getOk(),
				'note' => $value->getNote()
			];
		}
		return $data;
	}
}
