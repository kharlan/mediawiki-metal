<?php

namespace Kostajh\MwMetal\Status;

class StatusLookupValue {
	private string $name;
	private mixed $value;
	private int $ok;
	private ?string $note;

	public function __construct(
		string $name, mixed $value, int $ok = StatusInterface::STATUS_OK, ?string $note = null
	) {
		$this->name = $name;
		$this->value = $value;
		$this->ok = $ok;
		$this->note = $note;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getValue(): mixed {
		return $this->value;
	}

	/**
	 * @return int
	 */
	public function getOk(): int {
		return $this->ok;
	}

	/**
	 * @return string|null
	 */
	public function getNote(): ?string {
		return $this->note;
	}

}
